# work_with_pdf_and_doc



## Getting started

Required packages:

os, <br>
python-docx, <br>
xlwings, <br>
matplotlib.pyplot, <br>
string, <br>
subprocess, <br>
docx2pdf

## Beginning

- [ ] Install this packages:
```
pip install python-docx
pip install xlwings
pip install matplotlib
pip install string
pip install subprocess
pip install docx2pdf
```


## Description
This package is needed to make .pdf from an excel file and a workpiece in the form of a Word file and compress it by adding any formulas in LaTeX format to the content.



## Support
Tell me your problems in rasl98rasl@yandex.com 


## Authors and acknowledgment
Many thanks to the creators of the packages that were used for this project


## Project status
The project is completely completed
