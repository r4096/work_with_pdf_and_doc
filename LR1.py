import os
from docx import Document
from docx.shared import Pt
from docx.enum.text import WD_UNDERLINE
import xlwings as xw
import matplotlib.pyplot as plt
from docx.enum.text import WD_ALIGN_PARAGRAPH
import string
import subprocess
from docx2pdf import convert

# write your path to file LR2.docx and LR2.excl
rootdir = 'C:/ddd/PYprojects/PDFDOCX/'

# warning! you have to install gswin64 or other version typescript to work (use google)
gswin = 'C:\\Program Files\\gs\\gs9.56.1\\bin\\gswin64c.exe'

VRNT = 7
NAMEu = "Your_Name"
GROUP = "Your_Group"
PREPOD = "Your_Teacher_Name"
Year = "Year"

kachestvo = 4  # 1 2 3 4 5

vrnt1 = VRNT

if kachestvo == 5: kachestvo = 300
if kachestvo == 4: kachestvo = 250
if kachestvo == 3: kachestvo = 160
if kachestvo == 2: kachestvo = 100
if kachestvo == 1: kachestvo = 40

document = Document(f'{rootdir}LR2.docx')


def toFixed(numObj, digits=2):
    return f"{numObj:.{digits}f}"


def TPar(DocTabl, Ro, Ce, Paragraphs, Text=False):
    DD = document.tables[DocTabl].rows[Ro].cells[Ce].paragraphs[Paragraphs]
    if not Text:
        return DD
    else:
        return DD.text


def fontStyle(Paragraphs, FontSize=False, FontName=False, UnderLine=False, Bold=False):
    if (FontSize):
        Paragraphs.runs[0].font.size = Pt(FontSize)
    if (FontName):
        Paragraphs.runs[0].font.name = 'Times New Roman'
    if (UnderLine):
        Paragraphs.runs[0].font.underline = WD_UNDERLINE.SINGLE
    if (Bold):
        Paragraphs.runs[0].font.bold = True


def TValue(TablePar, Ch):
    TablePar.text = str(toFixed(float(Ch.value)))
    fontStyle(TablePar, 12, 1, 0, 0)


def PerList():
    p0 = TPar(1, 0, 3, 1)  # Name 22
    p1 = TPar(1, 1, 3, 1)  # Prepod22

    p2 = TPar(1, 0, 0, 0)  # group
    p3 = document.paragraphs[21]

    p0.text = p0.text.replace('Name22', NAMEu)
    p2.text = p2.text.replace('GROUP', GROUP)
    p1.text = p1.text.replace('Prepod22', PREPOD)
    p3.text = p3.text.replace('Year', Year)

    fontStyle(p0, 14, 1, 1)
    fontStyle(p1, 14, 1, 1)
    fontStyle(p2, 14, 1, 0)
    fontStyle(p3, 14, 1)


PerList()


def TabNum(a, b=False, DoValue=False):
    d = False
    a2 = string.ascii_lowercase.index(a[0])

    def StrInt(c):
        a11 = int(len(c))
        a2 = ''
        for i in range(1, a11):
            a2 += c[i]
        a2 = float(a2)
        a2 = int(a2)
        return a2

    a1 = StrInt(a)

    if (b == 0):
        d = 1
        if (DoValue == 1):
            c = sheet[a1 - 1, a2].value
            c = toFixed(c)
        else:
            c = sheet[a1 - 1, a2]

    if (b):
        b1 = StrInt(b)
        b2 = string.ascii_lowercase.index(b[0])
        a2 = (int(a2) + 1)
        b2 = (int(b2) + 1)
        if (DoValue):
            c = xw.Range((a1, a2), (b1, b2)).value
        else:
            c = xw.Range((a1, a2), (b1, b2))
            d = 1

    def perebor(c, g=2500):
        for i in range(len(c)):
            if (g < 2000):
                c[i][g] = c[i][g] = toFixed(c[i][g])
            if (g > 2000):
                c[i] = toFixed(c[i])

    if not d:
        if not a2 == b2:
            if not a1 == b1:
                dr = abs(a2 - b2) + 1

                for i in range(0, dr):
                    perebor(c, i)
        else:
            perebor(c)

    return c


p4 = document.paragraphs[26]
VRNT = str(VRNT)
p4.text = p4.text.replace('VRNT', VRNT)
fontStyle(p4, 14, 1, 0, 1)

# open EXCEL File
wb = xw.Book('LR2.xlsx')
sheet = wb.sheets[0]
VRNT = 21 + int(VRNT)

xw.Range((21, 2), (21, 6)).value = xw.Range((VRNT, 2), (VRNT, 6)).value

for i in range(0, 5):
    TValue(TPar(2, 1, i, 0, 0), sheet[16, i])
    p = TPar(2, 1, i, 0)
    p.text = str(int(float(p.text)))
    fontStyle(p, 12, 1)

dt2 = xw.Range('e2').value
iop = 0
tyty = []


def indOfPar(m=1, iop=0, jkjkjk=0):
    for i in range(iop, len(document.paragraphs)):

        if document.paragraphs[i].text.find('{' + str(m) + '}') != -1:
            tyty.insert(jkjkjk, i)
            p0 = document.paragraphs[int(tyty[0])]
            p0.text = p0.text.replace('{' + str(m) + '}', '')
            fontStyle(p0, 14, 1)
            jkjkjk = jkjkjk + i
            return int(i)


fig = plt.figure()


def TexPlot(tex, m=0, mm=0):
    document.paragraphs[m + mm - 1].insert_paragraph_before()
    tex = '$' + tex + '}$'
    fig.clear()
    ax = fig.add_axes([0, 0, 1, 1])
    ax.set_axis_off()
    k = str(mm)
    tex2x = 'test' + k + '.png'
    ### draw formula

    t = ax.text(0.5, 0.5, tex,
                horizontalalignment='center',
                verticalalignment='center',
                fontsize=12, color='black', style='italic')
    # , fontname='Times New Roman')

    ### Detect size of figure
    ax.figure.canvas.draw()
    bbox = t.get_window_extent()

    # set area of pic
    fig.set_size_inches(bbox.width / 80, bbox.height / 80)  # dpi=80
    ### draw
    plt.savefig(tex2x, dpi=kachestvo)
    p5 = document.paragraphs[m + mm - 1]
    p5.add_run().add_picture(tex2x)
    p5.alignment = WD_ALIGN_PARAGRAPH.CENTER
    fmt = p5.paragraph_format
    # space of paragr
    fmt.space_before = Pt(0)

    fmt.space_after = Pt(0)
    p5 = document.paragraphs[m + mm + 1].paragraph_format
    p5.space_before = Pt(0)

    p5.space_after = Pt(0)


f1 = str(TabNum('f17', 0, 1))
f2 = str(TabNum('a17', 0, 1))
f3 = str(TabNum('b2', 0, 1))
tex1 = 'S_{AM_{год}}=\\frac{' + f1 + '}{100}\cdot' + f2 + ' = ' + f3 + '\\  тыс. рублей'
lk = indOfPar(1)
TexPlot(tex1, 0, lk)

lk = indOfPar(2, iop)

g1 = TabNum('b17', 0, 1)
g1 = int(float(g1))

f1 = str(TabNum('g17', 0, 1))
f2 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):

    if (i > 1):
        f1 = str(TabNum('c' + str(i), 0, 1))
    f3c1 = i + 1
    f3c = 'c' + str(f3c1)
    f3 = str(TabNum(f3c, 0, 1))

    tex2 = 'E_{' + str(i) + '}=' + f1 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f2 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\  при \\ n = ' + str(i)

    TexPlot(tex2, lk, i)

lk = indOfPar(3, iop)

f1 = str(TabNum('a17', 0, 1))
for i in range(1, g1 + 1):

    if (i > 1):
        f1c = 'd' + str(i)
        f1 = str(TabNum(f1c, 0, 1))
    mk = i + 1
    f3c = 'b' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'd' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))

    tex2 = 'Ц_{ост.к.г(' + str(i) + ')}=' + f1 + '-' + f3 + '=' + f2 + ' \\ тыс. рублей'

    TexPlot(tex2, lk, i)

lk = indOfPar(3.1, iop)

f1 = str(TabNum('a17', 0, 1))
for i in range(1, g1 + 1):

    if (i > 1):
        f1c = 'd' + str(i)
        f1 = str(TabNum(f1c, 0, 1))
    mk = i + 1
    f3c = 'e' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'd' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Ц_{ср.год.ост(' + str(i) + ')}=\\frac{' + f1 + ' \\ +\\ ' + f2 + '}{2}=' + f3 + ' \\ тыс. рублей'
    TexPlot(tex2, lk, i)

lk = indOfPar(4, iop)

for i in range(1, g1 + 1):
    mk = i + 1
    f3c = 'e' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'f' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'H_{' + str(i) + '}=\\frac{2.2}{100} \\ \\cdot \\ ' + f3 + '=' + f2 + ' \\ тыс. рублей'
    TexPlot(tex2, lk, i)

lk = indOfPar(5, iop)

f1 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 1
    f3c = 'g' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'f' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Н_{диск(' + str(i) + ')}=' + f2 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f1 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\ тыс. рублей'
    TexPlot(tex2, lk, i)

lk = indOfPar(6, iop)
hj = 0
f1 = str(TabNum('u2', 0, 1))
for i in range(1, g1):
    mk = i + 1

    if (i > 1):
        f1c = 'a17'
        f1 = int(float(TabNum(f1c, 0, 1)))
        jkl = 'r' + str(i)
        jkl = int(float(TabNum(jkl, 0, 1)))
        f1 = f1 - jkl - hj
        f1 = str(f1)
        hj = hj + jkl

    f3c = 'f17'
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'r' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'S_{AM_{' + str(i) + '}}=2\\cdot \\frac{' + f3 + '}{100}\cdot' + f1 + ' = ' + f2 + '\\  тыс. рублей'
    TexPlot(tex2, lk, i)

lk = indOfPar(7, iop)

f1 = str(TabNum('a17', 0, 1))
for i in range(1, g1 + 1):

    if (i > 1):
        f1c = 't' + str(i)
        f1 = str(TabNum(f1c, 0, 1))
    mk = i + 1
    f3c = 'u' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 't' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Ц_{ср.год.ост(' + str(i) + ')}=\\frac{' + f1 + ' \\ +\\ ' + f2 + '}{2}=' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(8, iop)

f2 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 1
    f3c = 's' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f1c = 'r' + str(mk)
    f1 = str(TabNum(f1c, 0, 1))
    tex2 = 'E_{' + str(i) + '}=' + f1 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f2 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(9, iop)

for i in range(1, g1 + 1):
    mk = i + 1
    f3c = 'u' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'v' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'H_{' + str(i) + '}=\\frac{2.2}{100} \\ \\cdot \\ ' + f3 + '=' + f2 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(10, iop)

f1 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 1
    f3c = 'w' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'v' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Н_{диск(' + str(i) + ')}=' + f2 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f1 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(11, iop)
i = 0
f1 = str(TabNum('b17', 0, 1))
f2 = str(TabNum('h17', 0, 1))
f2 = str(int(float(f2)))
tex2 = '\sum Tocti=T_{A}\\frac{T_{A}+1}{2}=' + f1 + '\\frac{' + f1 + '+1}{2}=' + f2 + ' \\ тыс. руб.'
tex2x = 'test.png'
TexPlot(tex2, lk, i)

lk = indOfPar(11.1, iop)

f1 = str(TabNum('u2', 0, 1))
for i in range(1, g1):
    mk = i + 16
    hh = 1 - i
    f1 = float(TabNum('b17', 0, 1))
    f1 = str(toFixed(f1 - hh))
    f2 = str(TabNum('a17', 0, 1))
    f3 = str(TabNum('h17', 0, 1))

    f2c = 'k' + str(mk)
    f4 = str(TabNum(f2c, 0, 1))
    tex2 = 'S_{AM_{' + str(i) + '}}=\\frac{' + f2 + '}{' + f1 + '}\cdot' + f3 + '=' + f4 + ' \\ тыс. рублей'
    TexPlot(tex2, lk, i)

lk = indOfPar(12, iop)

f1 = str(TabNum('a17', 0, 1))
for i in range(1, g1 + 1):

    if (i > 1):
        f1c = 'm' + str(i + 16)
        f1 = str(TabNum(f1c, 0, 1))
    mk = i + 16
    f3c = 'n' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'm' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Ц_{ср.год.ост(' + str(i) + ')}=\\frac{' + f1 + ' \\ +\\ ' + f2 + '}{2}=' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(13, iop)

f2 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 16
    f3c = 'l' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f1c = 'k' + str(mk)
    f1 = str(TabNum(f1c, 0, 1))
    tex2 = 'E_{' + str(i) + '}=' + f1 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f2 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(14, iop)

for i in range(1, g1 + 1):
    mk = i + 16
    f3c = 'n' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'o' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'H_{' + str(i) + '}=\\frac{2.2}{100} \\ \\cdot \\ ' + f3 + '=' + f2 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(15, iop)

f1 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 16
    f3c = 'p' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'o' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Н_{диск(' + str(i) + ')}=' + f2 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f1 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(16, iop)
f1 = str(TabNum('f17', 0, 1))
f3 = str(TabNum('a17', 0, 1))
hj = 0
for i in range(1, g1 + 1):
    mk = i + 16
    hh = 1 + i
    f4c = 't' + str(mk)
    f4 = str(TabNum(f4c, 0, 1))

    if (i > 1):
        f1c = 'a17'
        f3 = int(float(TabNum(f1c, 0, 1)))
        jkl = 't' + str(mk - 1)
        jkl = int(float(TabNum(jkl, 0, 1)))
        f3 = f3 - jkl - hj
        f3 = str(f3)
        hj = hj + jkl
    tex2 = 'S_{нелинейн_{(' + str(i) + ')}}=2 \\cdot \\frac{' + f1 + '}{100}\\cdot ' + f3 + '=' + f4 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(17, iop)
f1 = str(TabNum('u2', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 16
    hh = 1 - i
    f1c = 't' + str(mk)
    f2c = 's' + str(mk)
    f3c = 'u' + str(mk)
    f1 = str(TabNum(f1c, 0, 1))
    f2 = str(TabNum(f2c, 0, 1))
    f3 = str(TabNum(f3c, 0, 1))
    tex2 = 'Эnpi_[(' + str(i) + ')] = \\left ( ' + f1 + ' - ' + f2 + ' \\right ) \\cdot 0.2 = ' + f3 + '  тыс. рублей'
    tex2x = 'test' + str(i) + '.png'
    TexPlot(tex2, lk, i)

lk = indOfPar(18, iop)
f1 = str(TabNum('c17', 0, 1))
for i in range(1, g1 + 1):
    mk = i + 16
    f3c = 'v' + str(mk)
    f3 = str(TabNum(f3c, 0, 1))
    f2c = 'u' + str(mk)
    f2 = str(TabNum(f2c, 0, 1))
    tex2 = 'Н_{(' + str(i) + ')}=' + f2 + '\\cdot \\frac{1}{\\left ( 1+\\frac{' + f1 + '}{100} \\right )^{' + str(
        i) + '}}= ' + f3 + ' \\ тыс. руб.'
    TexPlot(tex2, lk, i)

lk = indOfPar(19, iop)
i = 0
f1 = str(int(float(TabNum('e17', 0, 1))))
f2 = str(int(float(TabNum('d17', 0, 1))))
f3 = str(int(float(TabNum('b19', 0, 1))))
tex2 = 'Р=\\frac{П\cdot C}{100\%}= \\frac{' + f1 + '  \cdot ' + f2 + '}{100}  =  ' + f3 + ' \\  тыс.руб.'
TexPlot(tex2, lk, i)

f1 = str(int(float(TabNum('b19', 0, 1))))
f2 = str(int(float(TabNum('d17', 0, 1))))

lk = indOfPar(20, iop)
f3 = str((float(TabNum('u17', 0, 1))))
f4 = str(toFixed(float(TabNum('t29', 0, 0).value), 3))
tex2 = 'P_{1}=\\frac{' + f1 + ' - ' + f3 + '}{' + f2 + '+' + f3 + '} \\ = \\ ' + f4 + ' \%'
TexPlot(tex2, lk, i)

lk = indOfPar(21, iop)
f3 = str((float(TabNum('u18', 0, 1))))
f4 = str(toFixed(TabNum('t30', 0, 0).value, 3))

tex2 = 'P_{2}=\\frac{' + f1 + ' - ' + f3 + '}{' + f2 + '+' + f3 + '} \\ = \\ ' + f4 + ' \%'
TexPlot(tex2, lk, i)

g1 = TabNum('b17', 0, 1)
g1 = int(float(g1))
h1z = 0
h2z = 0
h3z = 0
h4z = 0
h5z = 0
for i in range(0, g1):
    h1z = h1z + sheet[16 + i, 11].value  # 1163
    h2z = h2z + sheet[1 + i, 2].value  # 1156
    h3z = h3z + sheet[1 + i, 6].value  # 122
    h4z = h4z + sheet[16 + i, 15].value  # 93
    h5z = h5z + sheet[16 + i, 21].value  # 23.04

lk = indOfPar(22, iop)
h1 = toFixed(float(h1z - h2z))
h1z = toFixed(float(h1z))
h2z = toFixed(float(h2z))
tex2 = '\\Delta АФ = \\ ' + h1z + ' -  ' + h2z + '  =  ' + h1 + ' \\ тыс.руб.'
TexPlot(tex2, lk, 0)

lk = indOfPar(23, iop)
h2 = toFixed(float(h3z - h4z))
h1z = toFixed(float(h3z))
h2z = toFixed(float(h4z))
tex2 = '\\Delta H_{ \\ им} = \\ ' + h1z + ' -  ' + h2z + '  =  ' + h2 + ' \\ тыс.руб.'
TexPlot(tex2, lk, 0)

lk = indOfPar(24, iop)
h1 = toFixed(float(float(h1) + float(h2) + float(h5z)))
h1z = toFixed(float(h1))
h2z = toFixed(float(h2))
h3z = toFixed(float(h5z))
tex2 = '\\Delta H_{ \\ им} = \\ ' + h1z + ' +  ' + h2z + '  +  ' + h3z + ' =  ' + h1 + ' \\ тыс.руб.'
TexPlot(tex2, lk, 0)

lk1 = indOfPar('ppv', iop)
document.paragraphs[lk1].alignment = WD_ALIGN_PARAGRAPH.CENTER
lk2 = indOfPar('ppv2', iop)
document.paragraphs[lk2].alignment = WD_ALIGN_PARAGRAPH.CENTER
lk3 = indOfPar('ppv3', iop)
document.paragraphs[lk3].alignment = WD_ALIGN_PARAGRAPH.CENTER
lk4 = indOfPar('ppv4', iop)
document.paragraphs[lk4].alignment = WD_ALIGN_PARAGRAPH.CENTER
if (g1 == 7): document.paragraphs[lk1].paragraph_format.page_break_before = True
if (g1 == 8): document.paragraphs[lk3].paragraph_format.page_break_before = True
if (g1 == 9): document.paragraphs[lk3].paragraph_format.page_break_before = True
if (g1 == 10): document.paragraphs[lk3].paragraph_format.page_break_before = True

g1 = 9
j1 = 0

for tabl in range(3, 7):
    tabl = int(tabl)
    dt22 = document.tables[tabl]

    for i in range(1, g1 + 1):  ### i - search row
        if (tabl < 6):
            j2 = 7
        else:
            j2 = 5
        dt22.add_row()

        for j in range(0, j2):  ### j - search cells

            p = dt22.rows[i].cells[j].paragraphs[0]
            TValue(TPar(tabl, i, j, 0), sheet[i, j + j1])
            if (j == 0):
                p.text = str(int(float(p.text)))
            else:
                p.text = p.text.replace('.', ',')
            p.alignment = WD_ALIGN_PARAGRAPH.CENTER
            fontStyle(p, 12, 1)
    if (tabl == 3): j1 = j1 + 16
    if (tabl > 3): j1 = j1 + 8

document.save('LR2r.docx')

convert(f"{rootdir}LR2r.docx", f"{rootdir}LR2r.pdf")

vr = f'{rootdir}' + str(vrnt1) + ' variant'
os.mkdir(vr)

quality = {
    0: '/default',
    1: '/prepress',
    2: '/printer',
    3: '/ebook',
    4: '/screen'
}

vr = f"{vr}/Name 2.pdf"
subprocess.call([gswin,
                 '-sDEVICE=pdfwrite',
                 '-dCompatibilityLevel=1.4',
                 '-dPDFSETTINGS={}'.format(quality[3]),
                 '-dNOPAUSE', '-dQUIET', '-dBATCH',
                 '-sOutputFile={}'.format(vr), f'{rootdir}LR2r.pdf'])

os.remove("LR2r.pdf")
os.remove("LR2r.docx")
